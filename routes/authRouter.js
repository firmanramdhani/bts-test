const express = require('express');
const router = express.Router();
const AuthController = require('../controller/authController')

router.post('/signup', AuthController.signup);


module.exports = router