var db = require("./db_config");

db.connect(function(err) {
    if (err) throw err;
    
    let sql_user = `CREATE TABLE user 
    (
        id int NOT NULL AUTO_INCREMENT,
        username VARCHAR(30),
        password VARCHAR(6),
        email VARCHAR(30),
        phone VARCHAR(30),
        country VARCHAR(30),
        city VARCHAR(30),
        postcode VARCHAR(30),
        name VARCHAR(255), 
        address VARCHAR(255),
        PRIMARY KEY (id)
    )`;
    db.query(sql_user, function (err, result) {
        if (err) throw err;
        console.log("Table user created");
    });


    let sql_shopping = `CREATE TABLE shopping 
    (
        id int NOT NULL AUTO_INCREMENT,
        name VARCHAR(255), 
        createdDate VARCHAR(255),
        PRIMARY KEY (id)
    )`;

    db.query(sql_shopping, function (err, result) {
        if (err) throw err;
        console.log("Table shopping created");
    });

});