require('dotenv').config();
const bcryptjs = require('bcryptjs');
const jsonwebtoken = require('jsonwebtoken')
const db = require("../database/db_config");

exports.signup = async (req, res) => {

    const username = req.body.users.username
    const email = req.body.users.email
    const password = req.body.users.encrypted_password
    const phone = req.body.users.phone
    const address = req.body.users.address
    const city = req.body.users.city
    const country = req.body.users.country
    const name = req.body.users.name
    const postcode = req.body.users.postcode
    

    const token = await jsonwebtoken.sign({
        username: username,
        email: email,
        password:password
    }, process.env.JWT_SECRET);


    
    db.connect(function(err) {
        if (err) throw err;
        
        
        let sql = `INSERT INTO user (username, password,email,phone,country,city,postcode,name,address) 
                   VALUES ('${username}', '${password}', '${email}','${phone}','${country}','${city}','${postcode}','${name}','${address}' )`;
    

        db.query(sql, function (err, result) {
            if (err) throw err;

            return res.status(201).json({
                status: true,
                email:email,
                token: token,
                users : username,
            })
            
        });
    });


}