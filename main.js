require('dotenv').config();
require('./database/db_config')
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const routerAuth = require('./routes/authRouter')



app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}))


app.use('/api/users/', routerAuth);

app.listen(process.env.PORT, (req, res) => {
    console.log(
        "server run at port " + process.env.PORT
    )
});